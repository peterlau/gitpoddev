use wasm_bindgen::prelude::*;

#[wasm_bindgen]
// call external function: DOM
extern "C" {
// extern {
    pub fn alert(s: &str);
    // private
    // fn alert(s: &str);
}

#[wasm_bindgen]
// this Rust function to be able to be called by JavaScript
pub fn greet(name: &str) {
    alert(&format!("Works, {}!", name));
}

