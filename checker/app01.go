// https://golangcode.com/basic-web-scraper/

package main

import (
	"fmt"
	// "strings"
	"log"
	"runtime"
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"pl.dev/goapp/checker"
)

func getTitles(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

  doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return "", err
	}
	titles := ""
	// doc.Find(".post-title").Each(func(i int, s *goquery.Selection) {
	// 	titles += "- " + s.Text() + "\n"
	// })

	runtime.GOMAXPROCS(4)
	resp_chan := make(chan checker.QueryResp, 10)

	var count []int
	doc.Find("tr").Each(func(i int, s *goquery.Selection) {
		var tds []string
		count = append(count, 0)
		s.Children().Slice(0, 2).Each(func(i int, s *goquery.Selection) {
			// fmt.Println(s.Text())
			tds = append(tds, s.Text())
		})
		// fmt.Println(strings.Join(tds, ":"))
		ip, port := tds[0], tds[1]
		fmt.Printf("ip_port: %s %s\n", ip, port)
		go checker.Query(ip, port, resp_chan)
	})

	for _, _ = range count {
		r := <-resp_chan 
		if r.Time > 1e-9 {
			fmt.Printf("%s %v\n", r.Addr, r.Time)
		}
	}
	return titles, nil
}

func fetch(url string) {
	titles, err := getTitles(url)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("Titles:")
	fmt.Println(titles)
}

func main() {
	// url := "https://golangcode.com"
	url := "https://www.kuaidaili.com/free/"
	fetch(url)
}
