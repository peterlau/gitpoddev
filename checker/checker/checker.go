// https://gist.github.com/sempr/de5559d0e3b99213dacf

package checker

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"runtime"
	"strings"
	"time"
)

type QueryResp struct {
	Addr string
	Time float64
}

func Query(ip string, port string, c chan QueryResp) {
	start_ts := time.Now()  // time start
	var timeout = time.Duration(15 * time.Second)
	host := fmt.Sprintf("%s:%s", ip, port)
	url_proxy := &url.URL{Host: host}
	client := &http.Client{
		Transport: &http.Transport{Proxy: http.ProxyURL(url_proxy)},
		Timeout: timeout,
	}
	// resp, err := client.Get("http://err.taobao.com/error1.html")
	// resp, err := client.Get("https://error.taobao.com/app/tbhome/common/error.html")
	resp, err := client.Get("http://www.baidu.com")
	if err != nil {
		fmt.Println("client error:")
		fmt.Println(err)
		c <- QueryResp{Addr: host, Time: float64(-1)}
		return
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	time_diff := time.Now().UnixNano() - start_ts.UnixNano()
	// fmt.Println(string(body))
	if strings.Contains(string(body), "alibaba.com") {
		c <- QueryResp{Addr: host, Time: float64(time_diff) / 1e9}
	} else {
		c <- QueryResp{Addr: host, Time: float64(-1)}
	}
}

func check() {
	runtime.GOMAXPROCS(4)
}

func run() {
	dat, _ := ioutil.ReadFile("ip.txt")
	dats := strings.Split(strings.TrimSuffix(string(dat), "\n"), "\n")
	runtime.GOMAXPROCS(4)

	resp_chan := make(chan QueryResp, 10)
	for _, addr := range dats {
		fmt.Println(addr)
		// split ip and port
		addrs := strings.SplitN(addr, string(' '), 2)
		ip, port := addrs[0], addrs[1]
		go Query(ip, port, resp_chan)
	}

	for _, _ = range dats {
		r := <-resp_chan 
		if r.Time > 1e-9 {
			fmt.Printf("%s %v\n", r.Addr, r.Time)
		}
	}
}
